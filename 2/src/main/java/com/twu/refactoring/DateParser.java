package com.twu.refactoring;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import static com.twu.refactoring.DateExceptionMessage.*;

public class DateParser {
    private final String dateAndTimeString;
    private static final HashMap<String, TimeZone> KNOWN_TIME_ZONES = new HashMap<String, TimeZone>();

    static {
        KNOWN_TIME_ZONES.put("UTC", TimeZone.getTimeZone("UTC"));
    }

    /**
     * Takes a date in ISO 8601 format and returns a date
     *
     * @param dateAndTimeString - should be in format ISO 8601 format
     *                          examples -
     *                          2012-06-17 is 17th June 2012 - 00:00 in UTC TimeZone
     *                          2012-06-17TZ is 17th June 2012 - 00:00 in UTC TimeZone
     *                          2012-06-17T15:00Z is 17th June 2012 - 15:00 in UTC TimeZone
     */
    public DateParser(String dateAndTimeString) {
        this.dateAndTimeString = dateAndTimeString;
    }

    public Date parse() {
        int year, month, date, hour, minute;

        year = parseTime(0, 4, TimeField.YEAR, 2000, 2012);

        month = parseTime(5, 7, TimeField.MONTH, 1, 12);

        date = parseTime(8, 10, TimeField.DATE, 1, 31);

        if ("Z".equals(dateAndTimeString.substring(11, 12))) {
            hour = 0;
            minute = 0;
        } else {
            hour = parseTime(11, 13, TimeField.HOUR, 0, 23);

            minute = parseTime(14, 16, TimeField.MINUTE, 0, 59);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(year, month - 1, date, hour, minute, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private int parseTime(int strStart, int strEnd, TimeField timeField, int minTime, int maxTime) {
        int time;
        String[] messages = parseExceptionMessage(timeField);
        try {
            String timeString = dateAndTimeString.substring(strStart, strEnd);
            time = Integer.parseInt(timeString);
        } catch (StringIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(messages[0]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(messages[1]);
        }
        if (time < minTime || time > maxTime)
            throw new IllegalArgumentException(messages[2]);
        return time;
    }



    private String[] parseExceptionMessage(TimeField field) {  // 这个是怎么抽的？？？
        switch (field){
            case YEAR:
                return new String[]{YEAR_STRING_IS_LESS_THAN_4_CHARACTERS, YEAR_IS_NOT_AN_INTEGER, YEAR_CANNOT_BE_LESS_THAN_2000_OR_MORE_THAN_2012};
            case MONTH:
                return new String[]{MONTH_STRING_IS_LESS_THAN_2_CHARACTERS, MONTH_IS_NOT_AN_INTEGER, MONTH_CANNOT_BE_LESS_THAN_1_OR_MORE_THAN_12};
            case DATE:
                return new String[]{DATE_STRING_IS_LESS_THAN_2_CHARACTERS, DATE_IS_NOT_AN_INTEGER, DATE_CANNOT_BE_LESS_THAN_1_OR_MORE_THAN_31};
            case HOUR:
                return new String[]{HOUR_STRING_IS_LESS_THAN_2_CHARACTERS, HOUR_IS_NOT_AN_INTEGER, HOUR_CANNOT_BE_LESS_THAN_0_OR_MORE_THAN_23};
            case MINUTE:
                return new String[]{MINUTE_STRING_IS_LESS_THAN_2_CHARACTERS, MINUTE_IS_NOT_AN_INTEGER, MINUTE_CANNOT_BE_LESS_THAN_0_OR_MORE_THAN_59};
            default:
                return new String[]{null, null, null};
        }
    }

}
