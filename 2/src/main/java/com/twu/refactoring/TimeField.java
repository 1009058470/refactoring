package com.twu.refactoring;

public enum TimeField {
    YEAR, MONTH, DATE, HOUR, MINUTE;
}
