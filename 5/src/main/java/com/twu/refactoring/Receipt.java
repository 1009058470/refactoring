package com.twu.refactoring;

public class Receipt {


    private static final double SALES_TAX_RATE = 0.1;

    private final Taxi taxi;

    public Receipt(Taxi taxi) {
        this.taxi = taxi;
    }

    public double getTotalCost() {
        // fixed charges
        double totalCost = Taxi.getBaseChange();

        // taxi charges
        int totalKms = taxi.getTotalKms();
        double peakTimeMultiple = taxi.getPeakTimeMultiple();
        totalCost = taxi.getTotalCost(totalCost, totalKms, peakTimeMultiple);

        return totalCost * (1 + SALES_TAX_RATE);
    }




}
