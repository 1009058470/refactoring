package com.twu.refactoring;

public class NumberCruncher {
    private final int[] numbers;

    public NumberCruncher(int... numbers) {
        this.numbers = numbers;
    }

    private int countOf(EventEnum eventEnum) {
        int count = 0;
        for (int number : numbers) {
            switch (eventEnum){
                case Odd:
                    if (number % 2 == 1) count++;
                    break;
                case Event:
                    if (number % 2 == 0) count++;
                    break;
                case Negative:
                    if (number >= 0) count++;
                    break;
                case Positive:
                    if(number < 0) count++;
                    break;
                default:
                    break;
            }

        }
        return count;
    }

    public int countEven() {
        return countOf(EventEnum.Event);
    }

    public int countOdd() {
        return countOf(EventEnum.Odd);
    }

    public int countPositive() {
        return countOf(EventEnum.Positive);
    }

    public int countNegative() {
        return countOf(EventEnum.Negative);
    }
}
