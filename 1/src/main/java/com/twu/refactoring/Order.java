package com.twu.refactoring;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Order {
    private static final String SALES_TAX = "Sales Tax";
    private static final String TOTAL_AMOUNT = "Total Amount";
    String orderName;
    String orderAddr;
    List<LineItem> orderList;

    public Order(String orderName, String orderAddr, List<LineItem> orderList) {
        this.orderName = orderName;
        this.orderAddr = orderAddr;
        this.orderList = orderList;
    }

    public String getCustomerName() {
        return orderName;
    }

    public String getCustomerAddress() {
        return orderAddr;
    }

    public List<LineItem> getLineItems() {
        return orderList;
    }

    AtomicReference<Double> getTotal() {
        AtomicReference<Double> total = new AtomicReference<>(0d);
        getLineItems().forEach(item -> {
            total.updateAndGet(v -> (double) (v + item.getTot()));
        });
        return total;
    }

    AtomicReference<Double> getTotalSalesTax() {
        AtomicReference<Double> totalSalesTax = new AtomicReference<>(0d);
        getLineItems().forEach(item -> {
            totalSalesTax.updateAndGet(v -> (double) (v + item.getSalesTax()));
        });
        return totalSalesTax;
    }

    public String getLineItemsAsString() {
        StringBuilder builder = new StringBuilder();
        getLineItems().forEach(item -> {
            builder.append(item.getInfoAsString());
        });
        return builder.toString();
    }

    public String getTotalSalesTaxAsString() {
        return SALES_TAX + '\t' + getTotalSalesTax();
    }

    public String getTotalAmountAsString() {
        return TOTAL_AMOUNT + '\t' + getTotal();
    }
}
