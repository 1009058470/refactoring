package com.twu.refactoring;

public class LineItem {
	private String description;
	private double price;
	private int quantity;

	public LineItem(String desc, double price, int quantity) {
		super();
		this.description = desc;
		this.price = price;
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public double getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

    double totalAmount() {
        return price * quantity;
    }

	double getSalesTax() {
		return totalAmount() * 0.10;
	}

	double getTot() {
		return totalAmount() + getSalesTax();
	}

	String getInfoAsString() {
		return getDescription() + "\t" +
				getPrice() + "\t" +
				getQuantity() + "\t" +
				totalAmount() + "\n";
	}
}